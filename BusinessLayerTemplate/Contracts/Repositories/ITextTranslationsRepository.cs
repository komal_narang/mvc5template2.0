// TODO: Reference common model.
namespace BusinessLayerTemplate.Contracts.Repositories
{
    public interface ITextTranslationsRepository
    {
        TextResponseModel GetTextByPage(int pageId, string language);
    }
}