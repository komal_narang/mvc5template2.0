﻿namespace BusinessLayerTemplate.Contracts.Services
{
    public interface ILocalizationServices
    {
        string GetTextById(int textId, string language, string domain);
    }
}