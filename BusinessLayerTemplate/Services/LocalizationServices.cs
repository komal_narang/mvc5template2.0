﻿using BusinessLayerTemplate.Contracts.Services;

namespace BusinessLayerTemplate.Services
{
    public class LocalizationServices : ILocalizationServices
    {
        private readonly ITextTranslationsRepository _textTranslationsRepository;
        private readonly ILogger _logger;

        public LocalizationServices(ITextTranslationsRepository textTranslationsRepository, ILogger logger)
        {
            _textTranslationsRepository = textTranslationsRepository;
            _logger = logger;
        }

        
        public string GetTextById(int textId, string language, string domain)
        {
            var translations = _textTranslationsRepository.GetTextByPage(0, language);

            var result = translations.Response.FirstOrDefault(m =>
                m.TextId == textId && m.DomainCode == domain)?.Text;

            //Try to read for default domain and return result.
            var text = result ?? translations.Response.FirstOrDefault(m => m.TextId == textId)?.Text;
            if (text == null)
            {
                //_logger.WriteError(Constants.ErrorCodes.Translation);
            }

            return text;
        }
    }
}