﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Web;

namespace CommonTemplate.Logger
{
    public class EventLogger : ILogger
    {
        private readonly EmailLogFailOver _emailLogFailOver;
        private readonly ILogStore _logStore;
        private readonly string _userHostAddress;

        public EventLogger(HttpContextBase httpContext, ILogStore logStore, EmailLogFailOver emailLogFailOver)
        {
            _logStore = logStore;
            _emailLogFailOver = emailLogFailOver;
            _userHostAddress = TryGetHostAddress(httpContext);
        }

        public void WriteError(string errorCode, string errorMessage, string additionalInfo, Exception exception,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            var log = InitializeLog(errorMessage, LogLevel.Error, memberName, sourceFilePath, sourceLineNumber);
            log.InnerException = exception?.InnerException?.Message;
            log.ErrorCode = errorCode;
            log.ExceptionMessage = exception?.Message;
            log.AdditionalInfo = additionalInfo;
            log.ExceptionType = exception?.GetType().Name;
            log.StackTrace = exception?.StackTrace;
            WriteOutput(log);
            SaveLog(log);
        }

        public void WriteWarning(string warningCode, string message, string additionalInfo,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            var log = InitializeLog(message, LogLevel.Warning, memberName, sourceFilePath, sourceLineNumber);
            log.AdditionalInfo = additionalInfo;
            log.ErrorCode = warningCode;
            WriteOutput(log);
            SaveLog(log);
        }

        public void WriteInfo(string message,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            var log = InitializeLog(message, LogLevel.Info, memberName, sourceFilePath, sourceLineNumber);
            WriteOutput(log);
            SaveLog(log);
        }

        public void WriteActivity(string message,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            var log = InitializeLog(message, LogLevel.Activity, memberName, sourceFilePath, sourceLineNumber);
            WriteOutput(log);
            SaveLog(log);
        }

        private void SaveLog(LogModel logModel)
        {
            try
            {
                WriteOutput(logModel);
                _logStore.Save(logModel);
            }
            catch (Exception ex)
            {
                _emailLogFailOver.SendOutEmail(ex);
            }
        }

        private LogModel InitializeLog(string message, LogLevel level, string memberName, string sourceFile,
            int lineNumber)
        {
            var log = new LogModel
            {
                Message = message,
                CallerMemberName = memberName,
                SourceLineNumber = lineNumber.ToString(),
                CallerFilePath = sourceFile,
                UserId = Thread.CurrentPrincipal.Identity.Name,
                IpAddress = _userHostAddress,
                Level = level.ToString(),
                LogDate = DateTime.UtcNow
            };

            return log;
        }

        private static string TryGetHostAddress(HttpContextBase httpContext)
        {
            try
            {
                return httpContext.Request.UserHostAddress;
            }
            catch
            {
                return null;
            }
        }

        private static void WriteOutput(LogModel logModel)
        {
#if DEBUG
            Debug.WriteLine($"[DATE]              : {logModel.LogDate}");
            Debug.WriteLine($"[LEVEL]             : {logModel.Level.ToUpper()}");
            Debug.WriteLine($"[MESSAGE]           : {logModel.Message}");
            Debug.WriteLine($"[LINE]              : {logModel.SourceLineNumber}");
            Debug.WriteLine($"[METHOD]            : {logModel.CallerMemberName}");
            Debug.WriteLine($"[FILE]              : {logModel.CallerFilePath}");

            if (logModel.Level == LogLevel.Error.ToString())
            {
                Debug.WriteLine($"[ERROR CODE]         : {logModel.ErrorCode}");
                Debug.WriteLine($"[EXCEPTION TYPE]     : {logModel.ExceptionType}");
                Debug.WriteLine($"[EXCEPTION MESSAGE]  : {logModel.ExceptionMessage}");
                Debug.WriteLine($"[INNER EXCEPTION]    : {logModel.InnerException}");
                Debug.WriteLine($"[ADDITIONAL INFO]    : {logModel.AdditionalInfo}");
                Debug.WriteLine($"[STACKTRACE]         : {logModel.StackTrace}");
            }

            Debug.WriteLine("*------------------------------------------------------------------------------*\n");
#endif
        }
    }
}