﻿using System.Collections.Generic;

namespace CommonTemplate.Logger
{
    public interface ILogStore
    {
        void Save(LogModel logModel);
        IEnumerable<LogModel> GetLogs(LogLevel level);
    }
}