﻿/****** Object:  Table [dbo].[SystemLog]    Script Date: 4/14/2018 10:13:29 PM ******/
/*
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SystemLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[Level] [nvarchar](15) NOT NULL,
	[ErrorCode] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[ExceptionType] [nvarchar](max) NULL,
	[ExceptionMessage] [nvarchar](max) NULL,
	[AdditionalInfo] [nvarchar](max) NULL,
	[StackTrace] [nvarchar](max) NULL,
	[InnerException] [nvarchar](max) NULL,
	[CallerMemberName] [nvarchar](max) NULL,
	[CallerFilePath] [nvarchar](max) NULL,
	[SourceLineNumber] [nvarchar](max) NULL,
	[UserId] [nvarchar](512) NULL,
	[IpAddress] [nvarchar](20) NULL,
 CONSTRAINT [PK_SystemLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
*/