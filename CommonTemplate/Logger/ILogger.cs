using System;
using System.Runtime.CompilerServices;

namespace CommonTemplate.Logger
{
    public interface ILogger
    {
        void WriteError(string errorCode, string errorMessage, string additionalInfo, Exception exception,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0);

        void WriteInfo(string message, [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0);

        void WriteActivity(string message, [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0);

        void WriteWarning(string warningCode, string message, string additionalInfo,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0);
    }
}