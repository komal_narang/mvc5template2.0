﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CommonTemplate.EmailClient;

namespace CommonTemplate.Logger
{
    public class EmailLogFailOver
    {
        private readonly IEmailClient _emailClient;

        public EmailLogFailOver(IEmailClient emailClient)
        {
            _emailClient = emailClient;
        }

        public void SendOutEmail(Exception ex)
        {
            var solution = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            var email = new Email
            {
                Receiver = ConfigurationProvider.QualityAssurance.QaEmails.ToList(),
                Subject = $"Exception Report - {solution}",
                Body =
                    $"Message = {ex.Message}\nStacktrace=  {ex.StackTrace}\n\n InnerExceptionMessage = {ex.InnerException?.Message}\n, InnerStack = {ex.InnerException?.StackTrace}"
            };

            _emailClient.Send(email);
        }
    }
}