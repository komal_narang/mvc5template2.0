﻿namespace CommonTemplate.Logger
{
    public enum LogLevel
    {
        Activity,
        Error,
        Info,
        Warning
    }
}