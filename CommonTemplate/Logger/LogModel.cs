using System;

namespace CommonTemplate.Logger
{
    public class LogModel
    {
        public long Id { get; set; }
        public string Level { get; set; }
        public string ExceptionType { get; set; }
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string InnerException { get; set; }
        public string AdditionalInfo { get; set; }
        public DateTime LogDate { get; set; }
        public string ExceptionMessage { get; set; }
        public string CallerMemberName { get; set; }
        public string CallerFilePath { get; set; }
        public string SourceLineNumber { get; set; }
        public string UserId { get; set; }
        public string IpAddress { get; set; }
    }
}