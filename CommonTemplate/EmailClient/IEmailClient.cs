﻿using System.Threading.Tasks;

namespace CommonTemplate.EmailClient
{
    public interface IEmailClient
    {
        void Send(Email emailModel);
        Task SendAsync(Email emailModel);
    }
}