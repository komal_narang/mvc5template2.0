﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using CommonTemplate.Exceptions;
using CommonTemplate.Logger;

namespace CommonTemplate.EmailClient
{
    public class EmailClient : IEmailClient
    {
        private readonly ILogger _logger;

        public EmailClient()
        {
        }

        public EmailClient(ILogger logger)
        {
            _logger = logger;
        }

        public void Send(Email email)
        {
            try
            {
                var client = GetSmptClient();
                var mail = GetMailMessage(email);
                client.Send(mail);
            }
            catch (Exception ex)
            {
                if (_logger == null) throw;
                _logger.WriteError(Constants.ErrorCodes.EmailClientFailed, Constants.ErrorMessages.EmailClientFailed, Json.Encode(email), ex);
                throw new HandledExceptions(ex);
            }
        }

        public async Task SendAsync(Email email)
        {
            try
            {
                using (var client = GetSmptClient())
                {
                    var mail = GetMailMessage(email);
                    await client.SendMailAsync(mail);
                }
            }
            catch (Exception ex)
            {
                if (_logger == null) throw;
                _logger.WriteError(Constants.ErrorCodes.EmailClientFailed, Constants.ErrorMessages.EmailClientAsyncFailed, Json.Encode(email), ex);
                throw new HandledExceptions(ex);
            }
        }

        private static SmtpClient GetSmptClient()
        {
            var client = new SmtpClient
            {
                UseDefaultCredentials = false,
                Host = ConfigurationProvider.Smtp.Server,
                Credentials = new NetworkCredential(ConfigurationProvider.Smtp.UserName, ConfigurationProvider.Smtp.Password),
                Timeout = ConfigurationProvider.Smtp.Timeout,
                DeliveryFormat = SmtpDeliveryFormat.International
            };

            return client;
        }

        private static MailMessage GetMailMessage(Email email)
        {
            var mail = new MailMessage
            {
                From = new MailAddress(ConfigurationProvider.Smtp.From, ConfigurationProvider.Smtp.FromDisplayName,
                    Encoding.UTF8),
                Subject = email.Subject,
                Body = email.Body,
                IsBodyHtml = email.IsHtml
            };

            if (ConfigurationProvider.QualityAssurance.EnalbeEmailBcc)
                foreach (var qaEmail in ConfigurationProvider.QualityAssurance.QaEmails)
                    mail.Bcc.Add(qaEmail);

            foreach (var emailAddress in email.Receiver)
                mail.To.Add(emailAddress);

            return mail;
        }
    }
}