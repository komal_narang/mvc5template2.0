﻿using System.Collections.Generic;

namespace CommonTemplate.EmailClient
{
    public class Email
    {
        public List<string> Receiver { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }
    }
}