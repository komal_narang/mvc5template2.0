﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChilindoTrivia.Common.Exceptions
{
    public class NotFoundInDbException : Exception
    {
        public NotFoundInDbException(string message) : base(message)
        {
        }
    }
}
