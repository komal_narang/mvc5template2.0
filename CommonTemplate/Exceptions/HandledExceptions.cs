﻿using System;

namespace CommonTemplate.Exceptions
{
    public class HandledExceptions : Exception
    {
        public HandledExceptions(Exception ex) : base(ex.Message)
        {
        }
    }
}