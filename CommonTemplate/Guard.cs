﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace CommonTemplate
{
    public static class Guard
    {
        private const string NotAvailable = "(not available)";

        public static void ThrowIfNull(object argumentValue, string argumentName,
            string exceptionMessage = null,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "")
        {
            if (argumentValue != null) return;

            if (string.IsNullOrEmpty(exceptionMessage))
                throw new ArgumentNullException(
                    $"Argument {argumentName} is null in {memberName} method in class {GetFileName(sourceFilePath)}.");

            throw new ArgumentNullException(
                $"Message: {exceptionMessage}. Argument {argumentName} is null in {memberName} method in class {GetFileName(sourceFilePath)}.");
        }

        public static void ThrowIfEmptyGuid(object argumentValue, string argumentName,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "")
        {
            if ((Guid)argumentValue != Guid.Empty) return;

            throw new ArgumentNullException(
                $"Argument {argumentName} is an empty Guid in {memberName} method in class {GetFileName(sourceFilePath)}.");
        }

        public static void ThrowIfNotFound(object argumentValue, string argumentName, object keyField,
            string exceptionMessage = null,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "")
        {
            if (argumentValue != null) return;


            // TODO: Include reference of common.exception
            //if (string.IsNullOrEmpty(exceptionMessage))
            //    throw new NotFoundInDbException($"Can not find {argumentName} with id {keyField}. Method {memberName} in class {GetFileName(sourceFilePath)}.");

            //throw new NotFoundInDbException($"{exceptionMessage} Can not find {argumentName} with id {keyField}. Method {memberName} in class {GetFileName(sourceFilePath)}.");
        }

        public static void ThrowIfCastFail(object argumentValue, string argumentName,
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "")
        {
            if (argumentValue != null) return;
            throw new InvalidCastException(
                $"Can not parse data as {argumentName} in {memberName} method at line {lineNumber} in file {GetFileName(sourceFilePath)}.");
        }

        private static string GetFileName(string sourceFilePath)
        {
            return !string.IsNullOrEmpty(sourceFilePath) ? new FileInfo(sourceFilePath).Name : NotAvailable;
        }
    }
}
