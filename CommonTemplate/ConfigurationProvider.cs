﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace CommonTemplate
{
    public static class ConfigurationProvider
    {
        private static class Keys
        {
            public const string SmtpServer = "MailgunSmtpServer";
            public const string SmtpUsername = "MailgunSmtpUsername";
            public const string SmtpPassword = "MailgunSmtpPassword";
            public const string MailFrom = "mailfrom";
            public const string MailFromDisplayName = "mailfromname";
            public const string SmtpTimeout = "SmtpTimeout";
            public const string QaEmailAddress = "QaEmailAddress";
            public const string EnableEmailBcc = "EnableEmailBcc";
            public const string CacheExpirationHour = "CacheExpirationHour";
        }

        public static class Smtp
        {
            static Smtp()
            {
                try
                {
                    Server = ConfigurationManager.AppSettings[Keys.SmtpServer];
                    UserName = ConfigurationManager.AppSettings[Keys.SmtpUsername];
                    Password = ConfigurationManager.AppSettings[Keys.SmtpPassword];
                    From = ConfigurationManager.AppSettings[Keys.MailFrom];
                    FromDisplayName = ConfigurationManager.AppSettings[Keys.MailFromDisplayName];
                    Timeout = int.Parse(ConfigurationManager.AppSettings[Keys.SmtpTimeout]);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(Constants.ErrorMessages.EmailConfigurations, ex);
                }
            }

            public static string Server { get; }
            public static string UserName { get; }
            public static string Password { get; }
            public static string From { get; }
            public static string FromDisplayName { get; }
            public static int Timeout { get; }
        }

        public static class BusinessRules
        {
            static BusinessRules()
            {
                try
                {
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(Constants.ErrorMessages.BusinessRulesConfigs, ex);
                }
            }
        }

        public class QualityAssurance
        {
            public static bool EnalbeEmailBcc;
            public static IEnumerable<string> QaEmails;

            static QualityAssurance()
            {
                try
                {
                    EnalbeEmailBcc = bool.Parse(ConfigurationManager.AppSettings[Keys.EnableEmailBcc]);
                    QaEmails = ConfigurationManager.AppSettings[Keys.QaEmailAddress].Split(';').Select(q => q.Trim())
                        .ToList();
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(Constants.ErrorMessages.QualityAssuranceConfigs, ex);
                }
            }
        }

        public static class Cache
        {
            public static int Hour;

            static Cache()
            {
                Hour = int.Parse(ConfigurationManager.AppSettings[Keys.CacheExpirationHour]);
            }
        }
    }
}