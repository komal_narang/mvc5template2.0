﻿namespace CommonTemplate
{
    public class Constants
    {
        public struct CrudResults
        {
            public static object GetSaveCompleteModel()
            {
                return new {Message = Message.SaveCompleteMessage, Result = Result.Success};
            }

            public static object GetSaveFailModel()
            {
                return new {Message = Message.SaveFailMessage, Result = Result.Fail};
            }

            public static object GetDeleteCompleteModel()
            {
                return new {Message = Message.DeleteCompleteMessage, Result = Result.Success};
            }

            public static object GetDeleteFailModel()
            {
                return new {Message = Message.DeleteFailMessage, Result = Result.Fail};
            }
        }

        public class Message
        {
            public const string SaveCompleteMessage = "Record saved successfully.";
            public const string SaveFailMessage = "Opps! Something went wrong. Record was not saved.";
            public const string DeleteCompleteMessage = "Record deleted successfully";
            public const string DeleteFailMessage = "Opps! Something went wrong. Record was not deleted.";
            public const string NotAvailable = "Not Available";
            public const string PleaseSelect = "--Please select--";
            public const string BadRequest = "Bad Request";
            public const string ServerError = "Internal Server Error.";
            public const string NullDisplayText = "Not Available";
            public static string UnexpectedError = "Opps! Something unexpected happend.";
            public static string ConfirmDeleteMessage = "Do you want to delete this record?";
        }

        public class ErrorMessages
        {
            public const string EmailConfigurations = "Could not read email configurations.";
            public const string UnexpectedError = "Whoops. Something went wrong.";

            public const string TrainingQuestionsNotFoundOrInvalidLanguageError =
                "Training Questions not found or invalid language passed as parameter.";

            public const string QualityAssuranceConfigs = "Failed to retrieve quality assurance configurations.";
            public const string EmailClientAsyncFailed = "Email client could not send email async.";
            public const string EmailClientFailed = "Email client could not send email.";
            public const string BusinessRulesConfigs = "Can not read business rules configurations from config file.";
        }

        public class ErrorCodes
        {
            public const string UnknownError = "EX20000";
            public const string EmailClientFailed = "EX10008";
            public static string EditAction = "UEx1000";
            public static string AddAction = "UEx1001";
            public static string SaveAction = "UEx1002";
            public static string DeleteAction = "UEx1003";
        }

        public static class WarningCodes
        {
            public const string BadRequest = "WR20000";
            public const string UnAuthorized = "WR20001";
            public static string NotFound = "WR20003";
        }

        public struct Status
        {
            public static string Active = "A";
            public static string Inactive = "I";
        }

        public class ActionName
        {
            public const string Edit = "Edit";
            public const string New = "New";
        }

        public class Result
        {
            public static string Success = "Success";
            public static string Fail = "Fail";
        }
    }
}