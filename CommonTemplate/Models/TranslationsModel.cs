﻿using System.Collections.Generic;

namespace DataAccessLayer.Models
{
    public class TextModel
    {
        public long ApplicationId { get; set; }
        public long PageId { get; set; }
        public int TextId { get; set; }
        public string TextIdentifier { get; set; }
        public string Placeholders { get; set; }
        public string DescriptionOfUse { get; set; }
        public string DomainCode { get; set; }
        public string LanguageCode { get; set; }
        public string Text { get; set; }
    }

    public class TextResponseModel
    {
        public List<TextModel> Response { get; set; }
        public long TotalRecordCount { get; set; }
        public bool Successful { get; set; }
        public string ResponseMessage { get; set; }
        public int ResponseCode { get; set; }
    }

    public class TextRequestModel
    {
        public int ApplicationId { get; set; }
        public int PageId { get; set; }
        public string LanguageCode { get; set; }
        public int TextId { get; set; }
        public string TextIdentifier { get; set; }
    }
}
