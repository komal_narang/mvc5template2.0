﻿namespace ChilindoTrivia.Entities.Dto
{
    public class FileUploadDto
    {
        public byte[] Buffer { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public float Size { get; set; }
    }
}
