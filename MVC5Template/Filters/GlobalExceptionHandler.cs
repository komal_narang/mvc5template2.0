﻿//TODO: Import references and uncomment code.

//using System.Net;
//using System.Web.Mvc;

//namespace MVC5Template.Filters
//{
//    public class GlobalExceptionHandler : ActionFilterAttribute
//    {
//        public override void OnResultExecuted(ResultExecutedContext filterContext)
//        {
//            switch (filterContext.HttpContext.Response.StatusCode)
//            {
//                case (int) HttpStatusCode.BadRequest:
//                    LogBadRequest(filterContext);
//                    break;
//                case (int) HttpStatusCode.Unauthorized:
//                    LogUnauthroized(filterContext);
//                    break;
//                case (int) HttpStatusCode.NotFound:
//                    LogNotFound(filterContext);
//                    break;
//                default:
//                    if (filterContext.HttpContext.Response.StatusCode != 200)
//                        LogExceptionDetail(filterContext);
//                    break;
//            }

//            base.OnResultExecuted(filterContext);
//        }

//        private static void LogExceptionDetail(ResultExecutedContext filterContext)
//        {
//            var actionName = GetActionName(filterContext);
//            var controllerName = GetControllerName(filterContext);
//            var responseMessage = GetResponseMessage(filterContext);
//            var ex = filterContext.Exception;
//            var serverResponseCode = filterContext.HttpContext.Response.StatusCode;
//            var logger = (ILogger) DependencyResolver.Current.GetService(typeof(ILogger));

//            if (filterContext.HttpContext.Response.StatusCode == (int) HttpStatusCode.Unauthorized)
//                logger.WriteInfo($"Login failed.");

//            else if (ex != null && ex.GetType() != typeof(HandledExceptions))
//                logger.WriteError(Constants.ErrorCodes.UnknownError,
//                    $"(GLobal) Unknow exception in action {actionName} controller {controllerName}. Server Response: {serverResponseCode}",
//                    responseMessage, ex);
//        }

//        private static void LogBadRequest(ResultExecutedContext filterContext)
//        {
//            var actionName = GetActionName(filterContext);
//            var controllerName = GetControllerName(filterContext);
//            var responseMessage = GetResponseMessage(filterContext);
//            var logger = (ILogger) DependencyResolver.Current.GetService(typeof(ILogger));

//            logger.WriteWarning(Constants.WarningCodes.BadRequest,
//                $"Bad Request in action {actionName} controller {controllerName}.", responseMessage);
//        }

//        private static void LogUnauthroized(ResultExecutedContext filterContext)
//        {
//            var requestMessage = GetRequestMessage(filterContext);
//            var responseMessage = GetResponseMessage(filterContext);
//            var logger = (ILogger) DependencyResolver.Current.GetService(typeof(ILogger));

//            logger.WriteWarning(Constants.WarningCodes.UnAuthorized, $"Authentication failed.",
//                $"Request : {requestMessage}, \n\nResponse:{responseMessage}");
//        }

//        private static void LogNotFound(ResultExecutedContext filterContext)
//        {
//            var logger = (ILogger) DependencyResolver.Current.GetService(typeof(ILogger));
//            var actionName = GetActionName(filterContext);
//            var controllerName = GetControllerName(filterContext);
//            var requestMessage = GetRequestMessage(filterContext);
//            var responseMessage = GetResponseMessage(filterContext);
//            logger.WriteWarning(Constants.WarningCodes.NotFound,
//                $"Not found exception in action {actionName} controller {controllerName}.",
//                $"Request : {requestMessage}, \n\nResponse:{responseMessage}");
//        }

//        private static string GetResponseMessage(ResultExecutedContext filterContext)
//        {
//            return filterContext.Result is ContentResult result ? result.Content : null;
//        }

//        private static string GetRequestMessage(ControllerContext filterContext)
//        {
//            return filterContext.HttpContext.Request.ToString();
//        }

//        private static string GetControllerName(ControllerContext filterContext)
//        {
//            return filterContext.RouteData.Values["controller"].ToString();
//        }

//        private static string GetActionName(ControllerContext filterContext)
//        {
//            return filterContext.RouteData.Values["action"].ToString();
//        }
//    }
//}