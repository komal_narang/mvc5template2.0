﻿//Ajax Methods.
var ConnectionTimeOut = 50000;

var ShowAjaxProgress = function () {
    const l = Ladda.create(document.querySelector(".ladda-sel"));
    l.start();
};

var HideAjaxProgress = function () {
    const l = Ladda.create(document.querySelector(".ladda-sel"));
    l.stop();
};

function BlockUI() {
    $.blockUI({
        message: '<img src="/Content/images/loading.gif" style="width:60px;height:60px;"/>',
        css: {
            backgroundColor: "transparent",
            border: "none"
        }
    });
}

function UnBlockUI() {
    $.unblockUI();
}

function HandleFailure(xhr, status, error) {
    console.log("xhr", xhr);
    console.log("status", status);
    console.log("error", error);
    console.log("xhr.status", xhr.status);

    if (xhr.readyState === 0) {
        ShowNotification("Connection timed out.", "error");
    } else {
        switch (xhr.status) {
            case 404:
                ShowNotification("Whoops! Not found.", "error");
                break;
            case 500:
                ShowNotification("Internal server error.", "error");
                break;
        }
    }
}

function SecureAjaxPostWithParameter(destinationUrl, functionSuccess) {
    const token = $('input[name="__RequestVerificationToken"]').val();
    const headers = {};
    headers["__RequestVerificationToken"] = token;
    $.ajax({
        type: "POST",
        url: destinationUrl,
        timeout: ConnectionTimeOut,
        data: { __RequestVerificationToken: token },
        dataType: "json",
        beforeSend: function () {
            BlockUI();
        },
        complete: function () {
            UnBlockUI();
        },
        success: function (response) {
            functionSuccess(response);
        },
        error: function (xhr, status, error) {
            HandleFailure(xhr, status, error);
        }
    });
}
// Ajax Methods Ends

//Popup Methods
function PopupForm(formId, formSourceUrl, size, labelYes, labelNo, labelYesClass, labelNoClass) {
    size = size || null;
    labelYes = labelYes || "Save";
    labelNo = labelNo || "Cancel";
    labelYesClass = labelYesClass || "btn-success ladda-button ladda-sel";
    labelNoClass = labelNoClass || "btn-default";

    $.ajax({
        url: formSourceUrl,
        type: "GET",
        timeout: ConnectionTimeOut,
        success: function (htmlContents) {
            bootbox.confirm({
                size: size,
                message: htmlContents,
                buttons: {
                    confirm: {
                        label: labelYes,
                        className: labelYesClass
                    },
                    cancel: {
                        label: labelNo,
                        className: labelNoClass
                    }
                },
                callback: function (isConfirmed) {
                    $(".ladda-sel").attr("data-style", "slide-down");
                    if (isConfirmed === true) {
                        $.validator.unobtrusive.parse($(document));
                        $(formId).validate();
                        if ($(formId).valid()) {
                            $(formId).submit();
                        }

                    } else {
                        console.log("form is invalid");
                        return true;
                    }
                    return false;
                }
            });
        },
        error: function (xhr, status, error) {
            HandleFailure(xhr, status, error);
        }
    });
}

function PopupFormWithFiles(sourceUrl,
    formId,
    destinationUrl,
    successCallBack,
    size,
    labelYes,
    labelNo,
    labelYesClass,
    labelNoClass) {
    var httpMethod = "POST";
    size = size || null;
    labelYes = labelYes || "Save";
    labelNo = labelNo || "Cancel";
    labelYesClass = labelYesClass || "btn-success ladda-button ladda-sel";
    labelNoClass = labelNoClass || "btn-default";

    $.ajax({
        url: sourceUrl,
        type: "GET",
        timeout: ConnectionTimeOut,
        success: function (htmlContents) {
            bootbox.confirm({
                message: htmlContents,
                size: size,
                buttons: {
                    confirm: {
                        label: labelYes,
                        className: labelYesClass
                    },
                    cancel: {
                        label: labelNo,
                        className: labelNoClass
                    }
                },
                callback: function (isConfirmed) {
                    if (isConfirmed === true) {
                        $(".ladda-sel").attr("data-style", "slide-down");
                        $.validator.unobtrusive.parse($(document));
                        $(formId).validate();

                        if ($(formId).valid()) {
                            const formdata = new FormData($("form").get(0));
                            $.ajax({
                                url: destinationUrl,
                                type: httpMethod,
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function () {
                                    ShowAjaxProgress();
                                },
                                complete: function () {
                                    HideAjaxProgress();
                                },
                                success: function (response) {
                                    successCallBack(response);
                                },
                                error: function (xhr, status, error) {
                                    HandleFailure(xhr, status, error);
                                }
                            });
                        }
                    } else { //User clicked close or cancel
                        return true;
                    }
                    return false;
                }
            });
        },
        error: function (xhr, status, error) {
            HandleFailure(xhr, status, error);
        }
    });
}

function PopupConfirm(title, message, callbackYes, callbackNo, labelYes, labelNo) {
    labelYes = labelYes || "Yes";
    labelNo = labelNo || "No";

    bootbox.confirm({
        title: title,
        message: message,
        buttons: {
            confirm: {
                label: labelYes,
                className: "btn-danger"
            },
            cancel: {
                label: labelNo,
                className: "btn-success"
            }
        },
        callback: function (result) {
            if (result) {
                callbackYes();
            } else {
                callbackNo();
            }
        }
    });
}

function PopupView(formSourceUrl, size) {
    if (size === undefined)
        size = null;

    $.ajax({
        url: formSourceUrl,
        type: "GET",
        success: function (htmlContents) {
            bootbox.alert({
                message: htmlContents,
                size: size
            });
        },
        error: function (xhr, status, error) {
            HandleFailure(xhr, status, error);
        }
    });
}

function PopupModelWithoutButtons(contents) {
    bootbox.dialog({
        message: contents,
        closeButton: false
    });
}

function PopupMessage(message, callback) {
    bootbox.alert({
        message: message,
        callback: function () {
            if (callback !== undefined) {
                callback();
            }
        }
    });
}

function HidePopup() {
    bootbox.hideAll();
}
//Popup Methods Ends.

function ShowNotification(text, messageType) {
    console.log("Shwoing notificaitons");
    toastr.options.fadeOut = 5000;
    if (messageType === "success") {
        toastr.success(text);
    } else if (messageType === "error") {
        toastr.error(text);
    } else {
        toastr(`Unknown toastr css class ${messageType}`);
        console.log(`invalid message type for shownotification method. value: ${messageType}`);
    }
}

//DOM Manipulation
function DeleteRecord(tableId, title, id, url, notificationArea, isDatatable) {

    PopupConfirm(title,
        "<h4>Are you sure you want to delete?</h4>",
        //Call back yes
        function () {
            const token = $('input[name="__RequestVerificationToken"]').val();
            const headers = {};
            headers["__RequestVerificationToken"] = token;

            $.ajax({
                type: "POST",
                url: url,
                timeout: ConnectionTimeOut,
                data: { __RequestVerificationToken: token, Id: id },
                dataType: "json",
                success: function (response) {
                    if (isDatatable === true) {
                        const table = $(`#${tableId}`).dataTable();
                        table.fnDeleteRow(`#Row${id}`);
                        table._fnReDraw();
                    } else {
                        DeleteDom(`#Row${id}`);
                    }

                    if (response.Result === RESULT_SUCCESS) {
                        ShowNotification(response.Message, NOTIFY_SUCCESS, notificationArea);
                    } else if (response.Result === RESULT_FAIL) {
                        ShowNotification(response.Message, NOTIFY_ERROR, notificationArea);
                    }
                },
                error: function (xhr, status, error) {
                    HandleFailure(xhr, status, error);
                }
            });
        },
        //callback No
        function () {
            HidePopup();
        }
    );
}

function DeleteDom(id) {
    console.log(`Deleting dom ${id}`);
    $(`#${id}`).remove();
}

function ResetDropDown(selector) {
    $(selector).prop("selectedIndex", 0);
}
//DOM Manipulation Ends.

function ApplyTypeAhead(targetId, minLength) {

    const sourceUrl = $(targetId).data("source-url");
    var destinationId = $(targetId).data("destination-id");

    if (minLength === undefined)
        minLength = 3;

    const bloodHoundSource = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace("text"),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: sourceUrl + "?q=%QUERY",
            wildcard: "%QUERY"
        }
    });

    $(targetId).typeahead({
        minLength: minLength,
        highlight: true
    },
        {
            name: "Id",
            display: "Text",
            source: bloodHoundSource,
            limit: 10
        }).on("typeahead:selected",
        function (e, suggestion, name) {
            // Set employee Id when user select item from typeahead list.
            $(destinationId).val(suggestion.Id);
        }).on("input",
        function (e) {
            //Set to employee Id whatever user types.
            const userInput = e.target.value;
            $(destinationId).val(userInput);
        });
}

function AddRowInDatatable(tableId, rowSourceUrl) {
    $.ajax({
        url: rowSourceUrl,
        type: "GET",
        timeout: ConnectionTimeOut,
        success: function (rowContents) {
            const newRow = $(rowContents);
            newRow.hide();
            const table = $(tableId).DataTable();
            console.log(newRow);
            table.row.add($(newRow)).draw();
            newRow.fadeIn(500);
        },
        error: function (xhr, status, error) {
            HandleFailure(xhr, status, error);
        }
    });
}


//Cookies
function SetCookie(key, value) {
    const expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + "=" + value + ";expires=" + expires.toUTCString() + ";path=/";

}

function GetCookie(key) {
    const keyValue = document.cookie.match(`(^|;) ?${key}=([^;]*)(;|$)`);
    return keyValue ? keyValue[2] : "undefined";
}

function RemoveCookie(key) {
    const value = "";
    console.log(`deletig cookie: ${key}=${value};expires=${new Date(0).toUTCString()};path=/`);
    document.cookie = key + "=" + value + ";expires=" + new Date(0).toUTCString() + ";path=/";
}

function AreCookiesEnabled() {
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;

    if (typeof navigator.cookieEnabled === "undefined" && !cookieEnabled) {
        document.cookie = "testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") !== -1) ? true : false;
    }
    return (cookieEnabled);
}

//Cookies ends
function IsFileSizeAcceptale(formId) {
    $(formId + " input:file").each(function () {
        console.log(this);
    });
}

function LoadDomByAjax(url, destionDomId) {
    $.ajax({
        url: url,
        beforeSend: function () {
            BlockUI();
        },
        complete: function () {
            UnBlockUI();
        },
        success: function (data) {
            $(`#${destionDomId}`).html(data);
        },
        error: function (xhr, status, error) {
            HandleFailure(xhr, status, error);
        }
    });
}


function BindReturnWithTextBoxButton(textboxId, buttonId) {
    $('#' + textboxId).keypress(function (e) {
        if (e.keyCode === 13) {
            $('#' + buttonId).click();
        }
    });
}

function PostAjaxFormWithFiles(formSelector, successCallBack) {
    if ($(formSelector).valid()) {
        var formdata = new FormData($(formSelector).get(0));
        console.log(formdata);
        var destinationUrl = $(formSelector).data("submit-url");
        $.ajax({
            url: destinationUrl,
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                ShowAjaxProgress();
            },
            complete: function () {
                HideAjaxProgress();
            },
            success: function (response) {
                successCallBack(response);
            },
            error: function (xhr, status, error) {
                HandleFailure(xhr, status, error);
            }
        });
    }
}
