﻿using Microsoft.Owin;
using MVC5Template;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace MVC5Template
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}