﻿using System.Web.Mvc;
using MVC5Template.Models;

namespace MVC5Template.Helpers
{
    public static class HtmlHelperExtension
    {
        public static MvcHtmlString TableSort(this UrlHelper url, string propertyName, PaginationViewModel filters)
        {
            return new MvcHtmlString(
                $"<div class=\"updown-arrow\">\n" +
                $"\t<p> <a id=\"{propertyName}-desc\" href=\'{url.Action(filters.ActionName, filters.ControllerName, new {page = filters.Page, filters.SearchKey, orderBy = propertyName, desc = true})}\'><i class=\"fa fa-sort-up\"></i></a></p>\n" +
                $"\t<p> <a id=\"{propertyName}\" href=\'{url.Action(filters.ActionName, filters.ControllerName, new {page = filters.Page, filters.SearchKey, orderBy = propertyName, desc = false})}\'><i class=\"fa fa-sort-down\"></i></a></p>\n" +
                $"</div>");
        }
    }
}