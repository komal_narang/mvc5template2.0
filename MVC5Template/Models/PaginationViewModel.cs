﻿namespace MVC5Template.Models
{
    public class PaginationViewModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public int Page { get; set; }
        public string OrderBy { get; set; }
        public string SearchKey { get; set; }
        public bool Desc { get; set; }
    }
}