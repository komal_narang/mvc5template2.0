﻿using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace MVC5Template
{
    public class IocConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule(new AutofacWebTypesModule());
            builder.Register(c => new HttpContextWrapper(HttpContext.Current)).As<HttpContextBase>().InstancePerLifetimeScope();

            #region Loggers
            //builder.RegisterType<DatabaseLogStore>().As(typeof(ILogStore)).InstancePerLifetimeScope();
            //builder.RegisterType<EventLogger>().As(typeof(ILogger)).InstancePerLifetimeScope();
            //builder.RegisterType<EmailLogFailOver>().As(typeof(EmailLogFailOver)).InstancePerLifetimeScope();
            #endregion

            #region BLL Services
            // builder.RegisterType<QuizMasterServices>().As(typeof(IQuizMasterServices));
            #endregion

            #region database
            //builder.RegisterType<TriviaContext>().As(typeof(TriviaContext));
            #endregion

            #region Repositories
            // builder.RegisterType<UserAnswerRepository>().As(typeof(IUserAnswerRepository));
            #endregion

            #region Caching
            //builder.RegisterType<CacheClient>().As(typeof(ICache)).InstancePerLifetimeScope();
            //builder.RegisterType<CachePublisher>().As(typeof(ICachePublisher));
            #endregion

            #region UserStore
            //builder.RegisterType<UserManager>().As(typeof(IUserManager));
            //builder.RegisterType<UserStore>().As(typeof(IUserStore));
            #endregion

            #region UnitOfWOrk
            //builder.RegisterType<UnitOfWork>().As(typeof(IUnitOfWork));
            #endregion
            var container = builder.Build();

            // Set the dependency resolver for MVC.
            var mvcResolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(mvcResolver);
        }
    }
}