﻿using System.Web.Optimization;

namespace MVC5Template
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/DataTables/jquery.dataTables.min.js",
                "~/Scripts/Global.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/libraries").Include(
                "~/Scripts/bootbox.js",
                "~/Scripts/bootbox.min.js",
                "~/Scripts/toastr.js",
                "~/Scripts/toastr.min.js",
                "~/Scripts/spin.min.js",
                "~/Scripts/ladda.min.js",
                "~/Scripts/Custom/Global.js",
                "~/Scripts/Custom/CommonScripts.js",
                "~/Scripts/chosen.jquery.js",
                "~/Scripts/chosen.jquery.min.js",
                "~/Scripts/jquery.blockUI.js",
                "~/Scripts/typeahead.bundle.js",
                "~/Scripts/typeahead.bundle.min.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.css",
                "~/Content/font-awesome.min.css",
                "~/Content/DataTables/css/jquery.dataTables.min.css",
                "~/Content/site.css",
                "~/Content/ladda-themeless.min.css",
                "~/Content/toastr.min.css",
                "~/Content/chosen.css",
                "~/Content/chosen.min.css",
                "~/Content/PagedList.css",
                "~/Content/toastr.css"));
        }
    }
}