////TODO: Fix localization services
//using System;
//using System.Configuration;
//using System.Runtime.Caching;
//using DataAccessLayer.Models;
//using Newtonsoft.Json;
//using RestSharp;

//namespace DataAccessLayer.LocalizationRepository
//{
//    public class TextTranslationsRepository : ITextTranslationsRepository
//    {
//        private readonly CacheItemPolicy _cacheItemPolicy;
//        private readonly MemoryCache _memoryCache;
//        private readonly ILogger _logger;

//        public TextTranslationsRepository(ILogger logger)
//        {
//            _logger = logger;
//            _memoryCache = MemoryCache.Default;
//            var hours = int.Parse(ConfigurationManager.AppSettings["CacheExpirationHour"]);
//            _cacheItemPolicy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(hours) };
//        }

//        public TextResponseModel GetTextByPage(int pageId, string language)
//        {
//            var cacheKey = $"Text-{pageId}-{language}";
//            if (TryGetFromCache(cacheKey, out var results)) return results;
//            results = SendRequest(language);
//            AddInCache(results, cacheKey);
//            return results;
//        }

//        private void AddInCache(TextResponseModel response, string key)
//        {
//            _memoryCache.Add(key, response, _cacheItemPolicy);
//        }

//        private bool TryGetFromCache(string key, out TextResponseModel results)
//        {
//            results = _memoryCache.Get(key) as TextResponseModel;
//            return results != null;
//        }

//        protected virtual TextResponseModel SendRequest(string language)
//        {
//            IRestResponse response = null;
//            RestRequest request = null;
//            try
//            {
//                var endPoint = ConfigurationProvider.Translations.EndPoint;

//                var requestModel = new TextRequestModel
//                {
//                    LanguageCode = language,
//                    ApplicationId = ConfigurationProvider.Translations.ApplicationId,
//                    TextIdentifier = string.Empty
//                };

//                request = new RestRequest(endPoint, Method.POST) { RequestFormat = DataFormat.Json };
//                request.AddBody(requestModel);

//                var client = new RestClient(endPoint);
//                response = client.Execute<TextResponseModel>(request);
//            }
//            catch (Exception ex)
//            {
//                _logger.WriteError(ErrorCodes.FailedToGetTranslations, $"{ErrorMessages.FailedToRetrive} {language}",
//                    $"Request Object {JsonConvert.SerializeObject(request)} \nServer Response: {JsonConvert.SerializeObject(response)}", ex);
//                throw new HandledExceptions(ex);
//            }

//            var results = JsonConvert.DeserializeObject<TextResponseModel>(response.Content);
//            if (results.Successful) return results;

//            _logger.WriteError(ErrorCodes.FailedToGetTranslations, $"{ErrorMessages.FailedToRetrive} {language}",
//                $"Status: {response.StatusCode} \n\nRequest Object {JsonConvert.SerializeObject(request)} \n\nServer Response: {JsonConvert.SerializeObject(response)}", null);
//            throw new HandledExceptions(new Exception("Failed to retrive langauges"));
//        }
//    }
//}